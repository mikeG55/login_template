from app import app
from user.models import User


@app.route('/user/signup', methods=['POST'])
def signup():
    user = User()
    return user.signup()


@app.route('/user/signout')
def signout():
    user = User()
    return user.signout()


@app.route('/user/login', methods=['POST'])
def login():
    user = User()
    return user.login()


@app.route('/user/request_password_reset', methods=['POST'])
def request_password_reset():
    user = User()
    return user.request_password_reset()


@app.route('/user/password_reset', methods=['POST'])
def password_reset():
    print("password_reset .... ")
    user = User()
    return user.password_reset()


@app.route('/user/resend_confirmation', methods=['POST'])
def resend_confirmation():
    print("In resend confirmation!!!!!")
    user = User()
    return user.resend_email_confirmation()


@app.route('/user/user_check', methods=['POST'])
def user_check():
    user = User()
    return user.username_check()
