from flask import flash, request, jsonify, session, redirect, render_template, url_for, Markup
from flask_mail import Message
from datetime import datetime
import uuid
import bcrypt
from config import config
from includes.helpers import generate_confirmation_token, get_reset_token, confirm_token
from app import mongo, mail


class User:

    @staticmethod
    def send_email(to, subject, template):
        msg = Message(subject, recipients=[to], html=template, sender=config['email']['username'])
        mail.send(msg)

    def start_session(self, user):

        del user['password']

        if not user['confirmed']:
            resend_confirmation = """
            This Account has not been verified. 
            <form class="form-container" name="resend_confirmation">
                <input type="hidden" name="email" value=%s id="inputEmail">
                <button type="submit">resend confirmation link</button>
            </form>    
            """ % user['email']
            flash(Markup(resend_confirmation), "warning")
            return jsonify(user), 200

        # update last login
        updated_last_login = {"$set": {"last_login": str(datetime.utcnow())}}
        mongo.db.users.update_one(user, updated_last_login)

        print("In Start Session ....")

        session['logged_in'] = True
        session['user'] = user
        print(user)

        return jsonify(user), 200

    def signup(self):
        # create user object.
        first_name = request.form.get("first_name")
        if not first_name:
            return jsonify({"error": "The First name field is required."}), 400

        last_name = request.form.get("last_name")
        if not last_name:
            return jsonify({"error": "The Last name field is required."}), 400

        email = request.form.get("email")
        if not email:
            return jsonify({"error": "The Email field is required."}), 400

        username = request.form.get("username")
        if not username:
            return jsonify({"error": "The Username field is required."}), 400

        password = request.form.get("password").encode("UTF-8")
        if not password:
            return jsonify({"error": "The Password field is required."}), 400

        confirm_password = request.form.get("confirm_password").encode("UTF-8")
        if not confirm_password:
            return jsonify({"error": "The Confirm Password field is required."}), 400

        if password != confirm_password:
            return jsonify({"error": "The Password field does not match the Confirm Password field."}), 400

        user = {
            "_id": uuid.uuid4().hex,
            "first_name": first_name,
            "last_name": last_name,
            "email": email,
            "username": username,
            "password": password,
            "last_login": str(datetime.utcnow()),
            "created": str(datetime.utcnow()),
            "confirmed": False
        }

        # encrypt the password.
        hashpass = bcrypt.hashpw(user['password'], bcrypt.gensalt())
        user['password'] = hashpass.decode("UTF-8")

        # check for existing email.
        if mongo.db.users.find_one({"email": user['email']}):
            return jsonify({"error": "Email address already used."}), 400

        print("Insertying user")

        if mongo.db.users.insert_one(user):
            print("Starting Session ....")
            # send confirmation email and show flash message
            token = generate_confirmation_token(user['email'])
            confirm_url = url_for('confirm_email', token=token, _external=True)
            html = render_template('activate.html', username=user['username'], email=user['email'],
                                   confirm_url=confirm_url)

            self.send_email(user['email'], "Please confirm your email", html)
            flash(Markup("Hi %s, To verify your account please click the confirmation link sent to your email address: <b>%s</b>" %
                         (user['username'], user['email'])), "success")
            return jsonify({"success": "Account verification needed."}), 200

        return jsonify({"error": "Signup Failed."}), 400

    def signout(self):
        session.clear()
        return redirect('/')

    def login(self):
        print(request.form.get("email"))
        user = mongo.db.users.find_one({
            "email": request.form.get("email")})

        if user and bcrypt.hashpw(request.form.get("password").encode("UTF-8"), user['password'].encode('utf-8')) == user['password'].encode('utf-8'):
            return self.start_session(user)

        return jsonify({"error": "Invalid login credentials."}), 401

    def request_password_reset(self):
        """

        :return:
        """
        email = request.form.get("email")
        if not email:
            return jsonify({"error": "The Email field is required."}), 400

        if not mongo.db.users.find_one({"email": email}):
            return jsonify({"error": "Email address does not exist."}), 400

        token = get_reset_token(email)
        reset_url = url_for('reset_email', token=token, _external=True)
        html = render_template('password_reset_email.html', reset_url=reset_url)
        subject = "Reset Your Password"
        self.send_email(email, subject, html)
        flash(Markup("Password reset link has been sent to your email address: <b>%s</b>" %
                     email), "success")
        return jsonify({"success": "Email sent."}), 200

    def password_reset(self):
        """

        :return:
        """
        token = request.form.get("token")
        email = confirm_token(token)

        if email is None:
            return jsonify({"error": "Failed to reset password."}), 400

        user = mongo.db.users.find_one({"email": email})
        if not user['confirmed']:
            return jsonify({"error": "Failed to reset password."}), 400

        # check the new password and confirm password are the same
        new_password = request.form.get("new_password").encode("UTF-8")
        if not new_password:
            return jsonify({"error": "The New Password field is required."}), 400

        confirm_password = request.form.get("confirm_password").encode("UTF-8")
        if not confirm_password:
            return jsonify({"error": "The Confirm Password field is required."}), 400

        if new_password != confirm_password:
            return jsonify({"error": "The New Password field does not match the Confirm Password field."}), 400

        # encrypt the password.
        hashpass = bcrypt.hashpw(new_password, bcrypt.gensalt())
        password = hashpass.decode("UTF-8")

        updated_password = {"$set": {"password": password}}
        if mongo.db.users.update_one(user, updated_password):
            html = render_template('password_changed_email.html', name=user['name'])
            self.send_email(email, 'Password Changed', html)
            flash("Your password has been reset, please login", "success")
            return jsonify({"success": "Password reset."}), 200

        return jsonify({"error": "Failed to reset password."}), 400

    def resend_email_confirmation(self):
        """

        :return:
        """
        email = request.form.get("email")
        # send confirmation email and show flash message
        token = generate_confirmation_token(email)
        confirm_url = url_for('confirm_email', token=token, _external=True)
        html = render_template('activate.html', confirm_url=confirm_url)
        subject = "Please confirm your email"
        self.send_email(email, subject, html)
        flash("To verify your account please click the confirmation link sent to your email address: %s" %
              email, "success")
        return jsonify({"success": "Confirmation email sent."}), 200

    def username_check(self):
        """

        :return:
        """
        username = request.form['username']
        print(username)
        if username and request.method == "POST":
            if mongo.db.users.find_one({"username": username}):
                resp = jsonify("<span style='color:red;'>Username unavailable</span>")
                resp.status_code = 200
                return resp
            else:
                resp = jsonify("<span style='color:green;'>Username available</span>")
                resp.status_code = 200
                return resp
        else:
            resp = jsonify("<span style='color:red;'>Username is required</span>")
            resp.status_code = 200
            return resp



