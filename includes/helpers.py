from flask import url_for, render_template
from itsdangerous import URLSafeTimedSerializer
from config import config


def confirm_token(token, expiration=1800):
    serializer = URLSafeTimedSerializer(config['app']['secret-key'])
    try:
        email = serializer.loads(token, salt=config['app']['SECURITY_PASSWORD_SALT'], max_age=expiration)
    except:
        email = None

    return email


def get_reset_token(email, expire_in_seconds=1800):
    """

    :param email: str
    :param expire_in_seconds: int (defauts 1800 30 minutes)
    :return:
    """
    serializer = URLSafeTimedSerializer(config['app']['secret-key'], expire_in_seconds)
    return serializer.dumps(email, salt=config['app']['SECURITY_PASSWORD_SALT'])


def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(config['app']['secret-key'])
    return serializer.dumps(email, salt=config['app']['SECURITY_PASSWORD_SALT'])


def send_confirmation_email(user, token):
    confirm_url = url_for('confirm_email', token=token, _external=True)
    html = render_template('activate.html', confirm_url=confirm_url)
    subject = "Please confirm your email"
    return user['email'], subject, html
    # send_email(user['email'], subject, html)