$("form[name=signup_form").submit(function(e) {

    var $form = $(this);
    var $error = $form.find(".error");
    var data = $form.serialize();

    $.ajax({
        url: "/user/signup",
        type: "POST",
        data: data,
        dataType: "json",
        success: function(resp) {
            window.location.href = "/create_account"
        },
        error: function(resp) {
            $error.text(resp.responseJSON.error).removeClass("error--hidden");
        }

    })

    e.preventDefault();
});

$("form[name=login_form").submit(function(e) {

    var $form = $(this);
    var $error = $form.find(".error");
    var data = $form.serialize();

    $.ajax({
        url: "/user/login",
        type: "POST",
        data: data,
        dataType: "json",
        success: function(resp) {
            console.log(resp);
            console.log(resp.confirmed);
            if (resp.confirmed) {
                window.location.href = "/dashboard/";
            } else {
                window.location.href = "/unconfirmed_account/";
            }

        },
        error: function(resp) {
            console.log(resp);
            $error.text(resp.responseJSON.error).removeClass("error--hidden");
        }

    })

    e.preventDefault();
});

$("form[name=resend_confirmation").submit(function(e) {

    var $form = $(this);
    var $error = $form.find(".error");
    var data = $form.serialize();

    $.ajax({
        url: "/user/resend_confirmation",
        type: "POST",
        data: data,
        dataType: "json",
        success: function(resp) {
            console.log(resp);
            window.location.href = "/"
        },
        error: function(resp) {
            console.log(resp);
            $error.text(resp.responseJSON.error).removeClass("error--hidden");
        }

    })

    e.preventDefault();
});

$("form[name=request_password_reset_form").submit(function(e) {

    var $form = $(this);
    var $error = $form.find(".error");
    var data = $form.serialize();

    $.ajax({
        url: "/user/request_password_reset",
        type: "POST",
        data: data,
        dataType: "json",
        success: function(resp) {
            console.log(resp);
            window.location.href = "/password_reset_request/"
        },
        error: function(resp) {
            console.log(resp);
            $error.text(resp.responseJSON.error).removeClass("error--hidden");
        }

    })

    e.preventDefault();
});

$('#new_password, #confirm_password').on('keyup', function () {
    if ($('#confirm_password').val()) {
        if ($('#new_password').val() == $('#confirm_password').val()) {
                $('#message').html('Matching').css('color', 'green');
        } else
            $('#message').html('Not Matching').css('color', 'red');
        }

});

$("form[name=password_reset_form").submit(function(e) {

    var $form = $(this);
    var $error = $form.find(".error");
    var data = $form.serialize();

    $.ajax({
        url: "/user/password_reset",
        type: "POST",
        data: data,
        dataType: "json",
        success: function(resp) {
            console.log(data);
            console.log(resp);
            window.location.href = "/"
        },
        error: function(resp) {
            console.log(resp);
            $error.text(resp.responseJSON.error).removeClass("error--hidden");
        }

    })

    e.preventDefault();
});

$('#username').on('input', function(e) {
    $('#msg').hide();
    $('#loading').show();
    if ($('#username').val() == null || $('#username').val() == "") {
        $('#msg').show();
        $('#msg').html("Username is required").css("color", "red");
        $('#loading').hide();
    } else {
        $.ajax({
        url: "/user/user_check",
        type: "POST",
        data: $("form[name=signup_form").serialize(),
        dataType: "html",
        cache: false,
        success: function(msg) {
            console.log(msg);
            $('#msg').show();
            $('#loading').hide();
            $('#msg').html(msg);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $('#msg').show();
            $('#loading').hide();
            $('#msg').html(textStatus + " " + errorThrown);
        }

    });

    }

  });

