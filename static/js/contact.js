//$(document).ready(function() {
//    $("#contact-form").validate({
//        rules: {
//            first_name: {
//                required: true,
//                 minlength: 3
//            },
//            last_name: {
//                 required: true,
//                 minlength: 3
//            },
//            email: {
//                required: true,
//                email: true
//            },
//            comment: {
//                required: true,
//                minlength: 3
//            }
//        },
//        messages : {
//            first_name: {
//                minlength: "Name should be at least 3 characters"
//            },
//            last_name: {
//                minlength: "Name should be at least 3 characters"
//            },
//            email: {
//                email: "The email should be in the format: abc@domain.tld"
//            }
//        }
//    });
//  });




/*// $(document).ready(function() {

    $('form').on('submit', function(event) {

        $.ajax({
            data: {
                first_name : $('first_name').val(),
                last_name : $('last_name').val(),
                email :  $('email').val(),
                comment :  $('comment').val()
            },
            type : "POST",
            url : '/process_contact'
        })
        .done(function(data) {
            console.log(data);
            if (data.error) {
                 $('#errorAlert').text(data.error).show();
                 $('#successAlert').hide();
            } else {
                 $('#successAlert').text(data.name).show();
                 $('#errorAlert').hide();
            }

        });

        event.preventDefault();
    });

});*/

$("form[name=contact_form").submit(function(e) {

     $.ajax({
            data: {
                first_name : $('#first_name').val(),
                last_name : $('#last_name').val(),
                email :  $('#email').val(),
                comment :  $('#comment').val()
            },
            type : "POST",
            url : '/process_contact',
            dataType : "json",
            success: function(resp) {
                console.log("In Success .... ")
                console.log(resp);
                $('#contact_form').trigger("reset");
                $('#successAlert').text(resp.success).show();
                $('#errorAlert').hide();
            },
            error: function(resp) {
                console.log("In error .... ")
                console.log(resp);
                $('#errorAlert').text(resp.responseJSON.error).show();
                $('#successAlert').hide();
            }

     })

    e.preventDefault();
});

