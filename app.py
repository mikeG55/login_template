from flask import Flask, render_template, request, redirect, session, flash, jsonify, Markup
from flask_mail import Mail
from functools import wraps
from datetime import datetime
from flask_pymongo import PyMongo
from config import config
from includes.helpers import confirm_token

# Create and name Flask app
app = Flask(__name__)
app.secret_key = config['app']['secret-key']

# database connection
app.config['MONGO_URI'] = config['db']['host']
mongo = PyMongo(app)

# email
# mail settings
app.config['MAIL_SERVER'] = '...'
app.config['MAIL_USERNAME'] = config['email']['username']
app.config['MAIL_PASSWORD'] = config['email']['password']
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)


# Decorators
def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            return redirect('/')

    return wrap


#  Routes
from user import routes


@app.route("/")
def index():
    if 'logged_in' in session:
        if session['logged_in']:
            return redirect('/dashboard/')
    return render_template('index.html')


@app.route("/create_account")
def create_account():
    if 'logged_in' in session:
        if session['logged_in']:
            return redirect('/dashboard/')
    return render_template('create_account.html')


@app.route("/dashboard/")
@login_required
def dashboard():
    return render_template('dashboard.html')


@app.route("/email_confirmation/")
def email_confirmation():
    return render_template('email_confirmation.html')


@app.route("/unconfirmed_account/")
def unconfirmed_account():
    return render_template('unconfirmed_account.html')


@app.route('/confirm/<token>')
def confirm_email(token):
    email = confirm_token(token)
    # email token may have expired.
    if email is None:
        flash(Markup("Email confirmation expired. <a href='/'>resend</a>"), "warning")
        return redirect('/')

    user = mongo.db.users.find_one({"email": email})
    if user['confirmed']:
        flash("Your account has been verified, please login", "success")
        return redirect('/')

    # update user confirmation
    updated_confirmation = {"$set": {"confirmed": True, 'confirmed_on': str(datetime.utcnow())}}
    mongo.db.users.update_one(user, updated_confirmation)
    flash("Your account has been verified, please login", "success")
    return redirect('/')


@app.route('/reset_email/<token>', methods=['GET', 'POST'])
def reset_email(token):
    email = confirm_token(token)
    # email token may have expired.
    if email is None:
        flash("This password reset link has expired.", "warning")
        return redirect('/')

    return render_template('password_reset.html', data=token)

@app.route("/password_reset_request/")
def password_reset_request():
    if 'logged_in' in session:
        if session['logged_in'] == True:
            return redirect('/dashboard/')

    return render_template('password_request_reset.html')








